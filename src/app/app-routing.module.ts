import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {path:'especialidades',
      loadChildren:()=>import('./especialidades/especialidades.module').then (m =>m.EspecialidadesModule),},
  {path:'carreras',
      loadChildren:()=>import('./carreras/carreras.module').then (m =>m.CarrerasModule),},
  {path:'marcas-laptop',
      loadChildren:()=>import('./marcas-laptop/marcas-laptop.module').then(m =>m.MarcasLaptopModule)},
  {path:'marcas-gaseosa',
      loadChildren:()=>import('./marcas-gaseosa/marcas-gaseosa.module').then(m =>m.MarcasGaseosaModule)},
  {path:'**', redirectTo:'especialidades'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
