import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CarrerasRoutingModule } from './carreras-routing.module';
import { SistemasComponent } from './sistemas/sistemas.component';
import { ArquitecturaComponent } from './arquitectura/arquitectura.component';
import { MedicinaComponent } from './medicina/medicina.component';


@NgModule({
  declarations: [
    MedicinaComponent,
    SistemasComponent,
    ArquitecturaComponent
  ],
  imports: [
    CommonModule,
    CarrerasRoutingModule
  ]
})
export class CarrerasModule { }
