import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MarcasGaseosaRoutingModule } from './marcas-gaseosa-routing.module';
import { CocaColaComponent } from './coca-cola/coca-cola.component';
import { FantaComponent } from './fanta/fanta.component';
import { SpriteComponent } from './sprite/sprite.component';


@NgModule({
  declarations: [
    CocaColaComponent,
    FantaComponent,
    SpriteComponent
  ],
  imports: [
    CommonModule,
    MarcasGaseosaRoutingModule
  ]
})
export class MarcasGaseosaModule { }
