import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CocaColaComponent } from './coca-cola/coca-cola.component';
import { FantaComponent } from './fanta/fanta.component';
import { SpriteComponent } from './sprite/sprite.component';

const routes: Routes = [
  {path:'',
    children:[
      {path:'CocaCola', component: CocaColaComponent},
      {path:'Fanta',component:FantaComponent},
      {path:'Sprite', component:SpriteComponent},
      {path:'**', redirectTo:'marcas-gaseosa'}
    ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarcasGaseosaRoutingModule { }
