import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AsusComponent } from './asus/asus.component';
import { DellComponent } from './dell/dell.component';
import { LenovoComponent } from './lenovo/lenovo.component';

const routes: Routes = [
  {path:'',
    children:[
      {path:'asus', component:AsusComponent},
      {path:'dell', component: DellComponent},
      {path:'lenovo', component: LenovoComponent},
      {path:'**', redirectTo:'marcas-laptop'}
    ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarcasLaptopRoutingModule { }
