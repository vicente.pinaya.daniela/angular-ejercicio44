import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MarcasLaptopRoutingModule } from './marcas-laptop-routing.module';
import { LenovoComponent } from './lenovo/lenovo.component';
import { DellComponent } from './dell/dell.component';
import { AsusComponent } from './asus/asus.component';


@NgModule({
  declarations: [
    LenovoComponent,
    DellComponent,
    AsusComponent
  ],
  imports: [
    CommonModule,
    MarcasLaptopRoutingModule
  ]
})
export class MarcasLaptopModule { }
