import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FrontEndComponent } from './front-end/front-end.component';
import { BackEndComponent } from './back-end/back-end.component';
import { FullStackComponent } from './full-stack/full-stack.component';



const routes: Routes= [
  {path:'',
   children:[
     {path:'frontEnd', component: FrontEndComponent},
     {path:'backEnd',component: BackEndComponent},
     {path:'fullStack',component:FullStackComponent},
     {path:'**', redirectTo:'login'}

   ]}
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class EspecialidadesRoutingModule { }
