import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FrontEndComponent } from './front-end/front-end.component';
import { BackEndComponent } from './back-end/back-end.component';
import { FullStackComponent } from './full-stack/full-stack.component';
import { EspecialidadesRoutingModule } from './especialidades-routing.module';



@NgModule({
  declarations: [
    FrontEndComponent,
    BackEndComponent,
    FullStackComponent
  ],
  imports: [
    CommonModule,
    EspecialidadesRoutingModule
  ]
})
export class EspecialidadesModule { }
